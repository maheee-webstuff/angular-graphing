import { Injectable } from '@angular/core';
import { DataService } from './data.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { GraphNode } from './graph.service';
import * as d3h from 'd3-hierarchy';

export { GraphNode } from './graph.service';

@Injectable()
export class D3GraphService {

  constructor(private dataService: DataService) {}

  getRootNode(): Observable<GraphNode> {
    return this.dataService.getData().pipe(map(data => {
      const width = 400, height = 400;

      const root: any = d3h.hierarchy(data.resources[0], currentNode =>
        data.resources.filter(node => 
          currentNode.members.map(member => member.value).indexOf(node.id) > -1
        )
      );

      const tree = d3h.tree().size([height, width - 100]);
      tree(root);

      return <GraphNode>root;
    }));
  }

}