import { Injectable } from '@angular/core';
import { DataService } from './data.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

export class GraphNode {
  x: number;
  y: number;
  children: GraphNode[];
  data?: any;
}

@Injectable()
export class GraphService {

  constructor(private dataService: DataService) {}

  getRootNode(): Observable<GraphNode> {
    return this.dataService.getData().pipe(map(data => {
      const items = data.resources;
      const tree: GraphNode = this.createTree(items);
      this.layout(tree);
      return tree;
    }));
  }

  private createTree(items: any[]): GraphNode {
    const root = this.createNode(items[0]);
    this.createChildrenTree(root, items);
    return root;
  }

  private createChildrenTree(parent: GraphNode, items: any[]) {
    for (const member of parent.data.members) {
      const childItem = this.findItem(items, member.value);
      if (childItem) {
        const child = this.createNode(childItem);
        parent.children.push(child);
        this.createChildrenTree(child, items);
      } else {
        console.log("NOT FOUND", member.value);
      }
    }
  }

  private createNode(item: any) {
    return {
      x: 0, y: 0,
      children: [],
      data: item
    }
  }

  private findItem(items, id) {
    for (const item of items) {
      if (item.id === id) {
        return item;
      }
    }
    return null;
  }

  private layout(node: GraphNode) {
    const maxDepthXMap = this.layoutAnalRec(node);
    this.layoutRec(node, maxDepthXMap);
  }

  private layoutAnalRec(node: GraphNode, depth: number = 0, depthXMap: number[] = []) {
    if (depthXMap.length <= depth) {
      depthXMap.push(0);
    }
    depthXMap[depth] = depthXMap[depth] + 1;
    for (const child of node.children) {
      this.layoutAnalRec(child, depth + 1, depthXMap);
    }
    return depthXMap;
  }

  private layoutRec(node: GraphNode, maxDepthXMap: number[], depth: number = 0, depthXMap: number[] = []) {
    if (depthXMap.length <= depth) {
      depthXMap.push(0);
    }
    const space = 140 / maxDepthXMap[depth];
    node.y = 180 * depth;
    node.x = space + depthXMap[depth] * 150;
    depthXMap[depth] = depthXMap[depth] + 1;
    for (const child of node.children) {
      this.layoutRec(child, maxDepthXMap, depth + 1, depthXMap);
    }
  }
}