import { Component, Input } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
//import { GraphService, GraphNode } from '../services/graph.service';
import { D3GraphService, GraphNode } from '../services/d3graph.service';

@Component({
  selector: 'graph',
  templateUrl: './graph.component.html',
  styleUrls: ["graph.component.scss"]
})
export class GraphComponent {

  @Input() width: number;
  @Input() height: number;

  nodes: Observable<GraphNode[]>;

  constructor(graphService: D3GraphService) {
    this.nodes = graphService.getRootNode().pipe(map(rootNode => {
      const result = [];
      const addNode = (node) => {
        result.push(node);
        if (node.children) {
          node.children.forEach(child => addNode(child))
        }
      };
      addNode(rootNode);
      return result;
    }));
  }

}
