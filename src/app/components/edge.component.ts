import { Component, Input, ChangeDetectionStrategy } from '@angular/core';
@Component({
  selector: '[graph-edge]',
  templateUrl: './edge.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class EdgeComponent {
  @Input() x1: number;
  @Input() y1: number;
  @Input() x2: number;
  @Input() y2: number;

  get d() {
    return '' +
      `M${this.y1},${this.x1}` +
      `C${this.y2 + 50},${this.x1}` +
      ` ${this.y2 + 150},${this.x2}` +
      ` ${this.y2},${this.x2}`;
    // 50 and 150 are coordinates of inflexion, play with it to change links shape
  }
}
