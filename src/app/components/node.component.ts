import { Component, Input, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: '[graph-node]',
  templateUrl: './node.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class NodeComponent {
  @Input() x: number;
  @Input() y: number;

  get transform() {
    return "translate(" + this.y + ", " + this.x + ")";
  }
}
