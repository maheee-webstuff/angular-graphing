import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { DataService } from './services/data.service';
import { GraphComponent } from './components/graph.component';
import { NodeComponent } from './components/node.component';
import { EdgeComponent } from './components/edge.component';
import { GraphService } from './services/graph.service';
import { D3GraphService } from './services/d3graph.service';

@NgModule({
  declarations: [
    AppComponent,
    GraphComponent,
    NodeComponent,
    EdgeComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [
    DataService,
    GraphService,
    D3GraphService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
